<?php

namespace Pcsg\Composer;

use Composer\Command\BaseCommand;
use Composer\Composer;
use Composer\Package\Package;
use Composer\Repository\RepositoryManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\Config\ConfigSourceInterface;

class TestCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('pcsg-test');
        $this->setDescription("Test please ignore");

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var Composer $composer */
        $composer = $this->getComposer();

        $conf = $composer->getConfig();
        $conf->getRepositories();

        foreach(Plugin::$packages As $pckg){
            $url = $pckg['repository'];

            if(!$this->repoExists($url)){
                /** @var ConfigSourceInterface $src */
                $src = $conf->getConfigSource();
                $arr = array(
                    "type" => "vcs",
                    "url" => $url
                );
                $src->addRepository("",$arr);
                $conf->setConfigSource($src);
            }
        }

    }

    private function repoExists($url){
        /** @var Composer $composer */
        $composer = $this->getComposer();

        $conf = $composer->getConfig();
        $repos = $conf->getRepositories();


        foreach($repos AS $repo){
            if($repo['url'] == $url){
                return true;
            }
        }
        return false;
    }

}