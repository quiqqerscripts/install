<?php

namespace Pcsg\Composer;

use Composer\Command\BaseCommand;
use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\Installer\InstallationManager;
use Composer\Package\Package;
use Composer\Repository\RepositoryInterface;
use Composer\Repository\RepositoryManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('pcsg-update');
        $this->setDescription("Updates supporting scripts for Quiqqer developers");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Composer $composer */
        $composer = $this->getComposer();

        exec("composer global update");

        $dir = $composer->getConfig()->get('vendor-dir');

        $qcommitFile = $dir . "/pcsg/git/src/PCSG/Git/qcommit.php";

        if (file_exists($qcommitFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }

            $output->writeln("copying " . $qcommitFile . " to " . $targetFolder . "/qcommit");
            copy($qcommitFile, $targetFolder . "/qcommit");
        }

        $qcreateFile = $dir . "/pcsg/git/src/PCSG/Git/qcreate.php";
        if (file_exists($qcreateFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }

            $output->writeln("copying " . $qcreateFile . " to " . $targetFolder . "/qcreate");
            copy($qcreateFile, $targetFolder . "/qcreate");
        }

        // *************************************************
        // Codesniffer
        /// ************************************************

        //Copy PHPCS to /home/{user}/bin
        $phpcsFile = $dir . "/squizlabs/php_codesniffer/scripts/phpcs";
        if (file_exists($phpcsFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";

            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }
            $targetFile = $targetFolder . "/phpcs";
            if (file_exists($targetFile)) {
                $output->writeln("Unlinking : ". $targetFile);
                unlink($targetFile);
            }
            $output->writeln("Linking " . $phpcsFile . " to " . $targetFile);
            symlink($phpcsFile, $targetFile);
            shell_exec("chmod +x " . $targetFile);
        }

        //Copy PHPcbf to /home/{user}/bin
        $phpcbfFile = $dir . "/squizlabs/php_codesniffer/scripts/phpcbf";
        if (file_exists($phpcbfFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }
            $targetFile = $targetFolder . "/phpcbf";

            if (file_exists($targetFile)) {
                $output->writeln("Unlinking : ". $targetFile);
                unlink($targetFile);
            }
            $output->writeln("Linking " . $phpcbfFile . " to " . $targetFile);
            symlink($phpcbfFile, $targetFile);
            shell_exec("chmod +x " . $targetFile);
        }
    }


    private function getHomeDir()
    {
        $home = "";

        $shell_user = posix_getpwuid(posix_getuid());
        $home       = $shell_user['dir'];

        if (empty($home)) {
            $home = getenv("HOME");
        }

        return $home;
    }
}
