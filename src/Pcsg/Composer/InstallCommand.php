<?php

namespace Pcsg\Composer;

use Composer\Command\BaseCommand;
use Composer\Composer;
use Composer\Config\ConfigSourceInterface;
use Composer\Package\Package;
use Composer\Repository\RepositoryManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('pcsg-install');
        $this->setDescription("Installs supporting scripts for Quiqqer developers");
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Composer $composer */
        $composer = $this->getComposer();

        /** @var RepositoryManager $repoMgr */
        $repoMgr = $composer->getRepositoryManager();
        /** @var Package $package */

        $conf = $composer->getConfig();
        $conf->getRepositories();

        # ###########################################
        # Repository
        # ###########################################

        foreach (Plugin::$packages as $pckg) {
            if (!isset($pckg['repository'])) {
                continue;
            }
            $url = $pckg['repository'];

            if (!$this->repoExists($url)) {
                /** @var ConfigSourceInterface $src */
                $src = $conf->getConfigSource();
                $arr = array(
                    "type" => "vcs",
                    "url"  => $url
                );
                $src->addRepository($pckg['name'], $arr);
                $output->writeln("Adding Custom Repository for Package : " . $pckg['name']);
                $conf->setConfigSource($src);
            }
        }

        # ###########################################
        # Require / Update
        # ###########################################

        foreach (Plugin::$packages as $pckg) {
            $name    = $pckg['name'];
            $version = $pckg['version'];

            exec("composer global require " . $name . ":" . $version);
        }


        # ###########################################
        # Copy Files
        # ###########################################


        $dir = $composer->getConfig()->get('vendor-dir');

        // Copy QCommit to /home/{user}/bin
        $qcommitFile = $dir . "/pcsg/git/src/PCSG/Git/qcommit.php";
        if (file_exists($qcommitFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }

            $output->writeln("copying " . $qcommitFile . " to " . $targetFolder . "/qcommit");
            copy($qcommitFile, $targetFolder . "/qcommit");
            shell_exec("chmod +x " . $targetFolder . "/qcommit");
        }

        // Copy QCreate to /home/{user}/bin
        $qcreateFile = $dir . "/pcsg/git/src/PCSG/Git/qcreate.php";
        if (file_exists($qcreateFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }

            $output->writeln("copying " . $qcreateFile . " to " . $targetFolder . "/qcreate");
            copy($qcreateFile, $targetFolder . "/qcreate");
            shell_exec("chmod +x " . $targetFolder . "/qcreate");
        }

        // *****************************************************
        // PHP Codesniffer
        // *****************************************************

        //Copy PHPCS to /home/{user}/bin
        $phpcsFile = $dir . "/squizlabs/php_codesniffer/scripts/phpcs";
        if (file_exists($phpcsFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";

            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }
            $targetFile = $targetFolder . "/phpcs";
            if (file_exists($targetFile)) {
                $output->writeln("Unlinking : ". $targetFile);
                unlink($targetFile);
            }
            $output->writeln("Linking " . $phpcsFile . " to " . $targetFile);
            symlink($phpcsFile, $targetFile);
            shell_exec("chmod +x " . $targetFile);
        }

        //Copy PHPcbf to /home/{user}/bin
        $phpcbfFile = $dir . "/squizlabs/php_codesniffer/scripts/phpcbf";
        if (file_exists($phpcbfFile)) {
            $targetFolder = $this->getHomeDir() . "/bin";
            if (!is_dir($targetFolder)) {
                mkdir($targetFolder);
            }
            $targetFile = $targetFolder . "/phpcbf";

            if (file_exists($targetFile)) {
                $output->writeln("Unlinking : ". $targetFile);
                unlink($targetFile);
            }
            $output->writeln("Linking " . $phpcbfFile . " to " . $targetFile);
            symlink($phpcbfFile, $targetFile);
            shell_exec("chmod +x " . $targetFile);
        }
    }


    private function getHomeDir()
    {

        $home = "";

        $shell_user = posix_getpwuid(posix_getuid());
        $home       = $shell_user['dir'];

        if (empty($home)) {
            $home = getenv("HOME");
        }

        return $home;
    }

    private function repoExists($url)
    {
        /** @var Composer $composer */
        $composer = $this->getComposer();

        $conf  = $composer->getConfig();
        $repos = $conf->getRepositories();


        foreach ($repos as $repo) {
            if ($repo['url'] == $url) {
                return true;
            }
        }

        return false;
    }
}
