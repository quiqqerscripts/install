# PCSG Composer

## General Information

This composer plugin helps you with installing and updating useful packages for Quiqqer developers.  
It installs the required repositories and takes further steps to integrate them into your system.  

Executable PHP-Scripts will be moved to ~/bin/ to make them executable globally for your user.  

### Important

The plugin will access your UNIX shell via PHP.  
Please make sure your PHP installation does have access to your Filesystem.

###Requirements 

These utilites require a UNIX-system.  
 
***

## Installation

If you do not have compsoer installed globally follow these instructions :

https://getcomposer.org/download/

Set Composer to allow packages in dev state 

    composer global config minimum-stability dev
    
Installation of the plugin is done via global composer.  

    composer global require "pcsg/devwizard":"dev-dev"
    
This will add a dependency to your global composer and starts the installation.

After the installation completes you are done with installing.

***

## Usage

To plugin will offer new commands

- pcsg-install : Installs the scripts
- pcsg-update : Updates the scripts.


####pcsg-install

    composer global pcsg-install

This will clone the package pcsg/git and install it.  
It will move the necessary scripts to your ~/bin/ directory to make them executable globally.  

> Important :
>   
> The scripts might not be callable right after the installation.  
> This usually happens, when your ~/bin folder didnt exist and had to be created.  
> The ~/bin folder does get included into your $PATH when you login to your system.

#### pcsg-update

    composer global pcsg-update
    
This will update the existing scripts.
 
***

## Troubleshooting

#### Scripts are not executable after setup
The scripts might not be callable right after the installation.  
This usually happens, when your ~/bin folder didnt exist and had to be created.  
The ~/bin folder does get included into your $PATH when you login to your system.

Please make sure, that your system includes the bin directory in your home directory to the systems $PATH.  

    cd ~
    nano .profile
    
Make sure following statements can be found :

    # set PATH so it includes user's private bin if it exists
    if [ -d "$HOME/bin" ] ; then
        PATH="$HOME/bin:$PATH"
    fi



